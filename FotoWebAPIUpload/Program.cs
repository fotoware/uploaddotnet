﻿// Example for uploading a file to FotoWeb through the API and applying metadata.
// For more information, see:
// http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_RESTful_API/Uploading_assets_using_the_API
// Copyright (C) 2015 FotoWare a.s.
// Released under the Microsoft Public License (MS-PL)
// See https://opensource.org/licenses/MS-PL or LICENSE.txt for terms and conditions.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace FotoWebAPIUpload
{
    // Helper classes for producing metadata JSON.
    // Always use a serializer to produce JSON rather than string operations.
    // This is the easiest way to avoid JSON injection vulnerabilities, because string values are automatically escaped.

    /// <summary>
    /// A metadata patch action without a value (e.g., "erase")
    /// </summary>
    public class MetadataFieldAction
    {
        public int id { get; set; }
        public string action { get; set; }
    }

    /// <summary>
    /// A metadata action with a single string value
    /// </summary>
    public class MetadataFieldActionWithValue : MetadataFieldAction
    {
        public string value { get; set; }
    }

    /// <summary>
    /// A metadata action with an array of string values (e.g., "add" on bag fields)
    /// </summary>
    public class MetadataFieldActionWithArray : MetadataFieldAction
    {
        public List<string> value { get; set; }
    }

    /// <summary>
    /// The whole metadata structure for an uploaded file
    /// </summary>
    public class MetadataActions
    {
        public List<MetadataFieldAction> fields { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // The following parameters must be edited to match your setup before you can run this example:

            // API authentication key
            // (See http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_RESTful_API/03_API_Authentication)
            var apiKey = "14jFf139V561Rkd4J395jX8f6A134Fd34Ed15";

            // Path on the local system to the file to upload
            var sourceFilePath = "C:\\path\\to\\file.jpg";

            // URL of folder or archive to upload to.
            // (See http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_RESTful_API/Ingestion)
            var destinationURL = "http://YOURSERVER/fotoweb/archives/5000-Upload/Upload/";

            // File name that the file shall have inside FotoWeb
            var destinationFileName = "file.jpg";

            // Metadata patch instructions
            var metadata = new MetadataActions
            {
                fields = new List<MetadataFieldAction>
                {
                    // This action sets a single value on a single value field
                    new MetadataFieldActionWithValue {id=5, action="add", value="Roadrunner"},
                    
                    // This action adds a single value to a multi-value field (preserving existing values)
                    new MetadataFieldActionWithValue {id=80, action="add", value="Wyle E. Coyote"},
                    
                    // This action replaces all values on a multi-value field with multiple new values
                    new MetadataFieldAction {id=25, action="erase"},
                    new MetadataFieldActionWithArray {id=25, action="add", value=new List<string> {"chicken", "food"}}
                }
            };

            // Set up the HTTP client
            var handler = new HttpClientHandler
            {
                CookieContainer = new CookieContainer(),
                UseCookies = true,
                UseDefaultCredentials = false,
                // Uncomment the following line to debug the request with Fiddler (Fiddler must be running)
                //Proxy = new WebProxy("http://localhost:8888", false, new string[] { }),
                UseProxy = true,
            };
            var client = new HttpClient(handler);

            // Set up request and headers
            var request = new HttpRequestMessage(HttpMethod.Post, destinationURL);
            request.Headers.Add("Accept", "application/vnd.fotoware.upload-result+json");
            // API authentication. Replace value with a valid API key
            request.Headers.Add("FWAPIToken", apiKey);
            
            // Upload requests are multipart/form-data requests
            var content = new MultipartFormDataContent();
            request.Content = content;

            // Add a file to upload
            // 'name' must be "Filedata", and 'filename' must be the name that the file will have inside FotoWeb.
            content.Add(new StreamContent(File.Open(sourceFilePath, FileMode.Open)), "Filedata", destinationFileName);

            // Serialize the metadata structure to a JSON string
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var metadataJSON = serializer.Serialize(metadata);

            // Add the JSON data to the request
            // 'name' must be "Metadata", and 'filename' must be the 'filename' of the asset file plus ".metadata.json".
            content.Add(new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(metadataJSON)), "Metadata", destinationFileName + ".metadata.json");

            // Send the request and get the result
            var result = client.SendAsync(request);

            // Dump the result
            Console.WriteLine(result.Result.ToString());

            // Dump the returned JSON data
            Console.WriteLine(result.Result.Content.ReadAsStringAsync().Result);
        }
    }
}
