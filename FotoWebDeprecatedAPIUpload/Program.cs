﻿// Example for uploading a file to FotoWeb through the API and applying metadata.
// For more information, see:
// http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_RESTful_API/Uploading_assets_using_the_API
// Copyright (C) 2015 FotoWare a.s.
// Released under the Microsoft Public License (MS-PL)
// See https://opensource.org/licenses/MS-PL or LICENSE.txt for terms and conditions.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.Xml;

namespace FotoWebDeprecatedAPIUpload
{
    // Force UTF-8
    public sealed class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }

    // Helper classes for producing metadata XML.
    // Always use a serializer to produce XML rather than string operations.
    // This is the easiest way to avoid XML injection vulnerabilities, because string values are automatically escaped.
    [XmlInclude(typeof(MetadataFieldAction)), XmlInclude(typeof(MetadataFieldActionWithValue)), XmlInclude(typeof(MetadataFieldWithValue))]

    [Serializable]
    public abstract class Field
    {
        [XmlAttribute]
        public int Id { get; set; }
    }

    /// <summary>
    /// A metadata field with a single string value
    /// </summary>
    [Serializable]
    public class MetadataFieldWithValue : Field
    {
        [XmlText]
        public string Value { get; set; }
    }

    /// <summary>
    /// A metadata patch action without a value (e.g., "erase")
    /// </summary>
    [Serializable]
    public class MetadataFieldAction : Field
    {
        [XmlAttribute]
        public string Action { get; set; }
    }

    /// <summary>
    /// A metadata action with a single string value
    /// </summary>
    [Serializable]
    public class MetadataFieldActionWithValue : MetadataFieldAction
    {
        [XmlText]
        public string Value { get; set; }
    }

    /// <summary>
    /// The whole metadata structure for an uploaded file
    /// </summary>
    [Serializable]
    [XmlRoot("Text")]
    public class Metadata : List<Field>
    {
    }

    class Program
    {
        static void Main(string[] args)
        {
            // The following parameters must be edited to match your setup before you can run this example:

            // API authentication key
            // (See http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_RESTful_API/03_API_Authentication)
            var apiKey = "14jFf139V561Rkd4J395jX8f6A134Fd34Ed15";

            // Path on the local system to the file to upload
            var sourceFilePath = "C:\\path\\to\\file.jpg";

            // URL of folder or archive to upload to.
            // (See http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_RESTful_API/Ingestion)
            var destinationURL = "http://YOURSERVER/fotoweb/archives/5000-YOURARCHIVE/ARCHIVEFOLDER/SUBFOLDER/";

            // File name that the file shall have inside FotoWeb
            var destinationFileName = "file.jpg";

            // Metadata patch instructions
            var metadata = new Metadata
            {
                // This action sets a single value on a single value field
                new MetadataFieldActionWithValue {Id=5, Action="add", Value="Roadrunner"},
                    
                // This action adds a single value to a multi-value field (preserving existing values)
                new MetadataFieldActionWithValue {Id=80, Action="add", Value="Wyle E. Coyote"},
                    
                // This action replaces all values on a multi-value field with multiple new values
                new MetadataFieldAction {Id=25, Action="erase"},
                new MetadataFieldActionWithValue {Id=25, Action="add", Value="chicken"},
                new MetadataFieldActionWithValue {Id=25, Action="add", Value="food"},

                // This replaces the string value with a new value
                new MetadataFieldWithValue {Id=120, Value="This is a description"},
            };

            // Set up the HTTP client
            var handler = new HttpClientHandler
            {
                CookieContainer = new CookieContainer(),
                UseCookies = true,
                UseDefaultCredentials = false,
                // Uncomment the following line to debug the request with Fiddler (Fiddler must be running)
                //Proxy = new WebProxy("http://localhost:8888", false, new string[] { }),
                UseProxy = true,
            };
            var client = new HttpClient(handler);

            // Set up request and headers
            var request = new HttpRequestMessage(HttpMethod.Post, destinationURL);
            request.Headers.Add("Accept", "application/vnd.fotoware.upload-result+json");
            // API authentication. Replace value with a valid API key
            request.Headers.Add("FWAPIToken", apiKey);

            // Upload requests are multipart/form-data requests
            var content = new MultipartFormDataContent();
            request.Content = content;

            // Add a file to upload
            // 'name' must be "Filedata", and 'filename' must be the name that the file will have inside FotoWeb.
            content.Add(new StreamContent(File.Open(sourceFilePath, FileMode.Open)), "Filedata", destinationFileName);

            // Serialize the metadata structure to a XML string
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(metadata.GetType());
            // Remove xsi and xsd from XML
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            StringWriter writer = new Utf8StringWriter();
            serializer.Serialize(writer, metadata, emptyNamepsaces);
            var metadataXML = writer.ToString();

            // Add the XML data to the request
            // Content-Disposition: form-data; name="metadata:image.jpg".
            content.Add(new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(metadataXML)), "metadata:" + destinationFileName);

            // Send the request and get the result
            var result = client.SendAsync(request);

            // Dump the result
            Console.WriteLine(result.Result.ToString());

            // Dump the returned JSON data
            Console.WriteLine(result.Result.Content.ReadAsStringAsync().Result);
        }
    }
}
