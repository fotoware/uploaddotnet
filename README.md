# README #

Example for uploading a file to FotoWeb through the API and applying metadata.

For more information, see [the API documentation](http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/The_FotoWeb_RESTful_API/Uploading_assets_using_the_API)


## Requirements ##

* Microsoft Visual Studio 2012
* A FotoWeb server
* An API key for the FotoWeb server
* Upload access to at least one archive (if the API key is not an admin key)
* A file to upload

## How to run this example ##

* Open the solution (.sln) file in Microsoft Visual Studio 2012
* Change the parameters at the beginning of the Main function to suit your setup
* Run the example

## How do I integrate this code into my own? ##

You are free to use this code in your own proprietary products. Please see LICENSE.txt for more details.